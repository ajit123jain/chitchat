package ajit123jain.chitchat.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;

import ajit123jain.chitchat.BuildConfig;
import ajit123jain.chitchat.R;
import butterknife.ButterKnife;
import timber.log.Timber;

public class SignInActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 700;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        boolean isSmartLockEnabled = !BuildConfig.DEBUG;
        auth = FirebaseAuth.getInstance();
        startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setTheme(R.style.FirebaseUITheme)
                        .setLogo(R.drawable.splash_logo)
                        .setAvailableProviders(
                                Arrays.asList(new AuthUI.IdpConfig.GoogleBuilder().build(),
                                        new AuthUI.IdpConfig.PhoneBuilder().build(),
                                        new AuthUI.IdpConfig.EmailBuilder().build())
                        )
                        .setIsSmartLockEnabled(isSmartLockEnabled)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                Timber.d("Provider type: %s\nUser email: %s\nUser phone: %s", response.getProviderType(), response.getEmail(), response.getPhoneNumber());

                Intent intentToMainActivity = new Intent(SignInActivity.this, MainActivity.class);
                intentToMainActivity.putExtra("firebase_token", response.getIdpToken());
                startActivity(intentToMainActivity);
                finish();
                return;
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    showToast("Sign-in canceled.");
                    finish();
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showToast("Please connect to the Internet and try again.");
                    finish();
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    showToast("Unable to login. Please try again!");
                    finish();
                    return;
                }
            }

            showToast("Unknown error. Please try again!");
            finish();
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}

