package ajit123jain.chitchat.activities;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

import ajit123jain.chitchat.R;

import static com.heinrichreimersoftware.materialintro.app.IntroActivity.BUTTON_BACK_FUNCTION_BACK;
import static com.heinrichreimersoftware.materialintro.app.IntroActivity.BUTTON_NEXT_FUNCTION_NEXT_FINISH;

public class IntroActivity extends com.heinrichreimersoftware.materialintro.app.IntroActivity{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullscreen(true);
        setButtonBackVisible(true);
        setButtonBackFunction(BUTTON_BACK_FUNCTION_BACK);
        setButtonNextVisible(true);
        setButtonNextFunction(BUTTON_NEXT_FUNCTION_NEXT_FINISH);

        addSlide(new SimpleSlide.Builder()
                .background(R.color.md_deep_purple_700)
                .backgroundDark(R.color.md_deep_purple_900)
                .image(R.drawable.img_placeholder)
                .title("Welcome to HealthGraph's Silfra app!")
                .description("We provide information from your doctor to you, on time and instantly. (DRAFT)")
                .scrollable(true)
                .build());

        addSlide(new SimpleSlide.Builder()
                .background(R.color.md_deep_orange_700)
                .backgroundDark(R.color.md_deep_orange_900)
                .image(R.drawable.img_notifications_placeholder)
                .title("Get notified about your prescriptions!")
                .description("We'll send you notifications from your doctor about your prescriptions! (DRAFT)")
                .scrollable(true)
                .build());

        String[] permissionsNeeded = { Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };
        addSlide(new SimpleSlide.Builder()
                .background(R.color.md_lime_500)
                .backgroundDark(R.color.md_lime_700)
                .image(R.drawable.img_permissions)
                .title("Take heart rate readings from your phone!")
                .description("But first, we need you to give us certain permissions. (DRAFT)")
                .scrollable(true)
                .permissions(permissionsNeeded)
                .build());

        addSlide(new SimpleSlide.Builder()
                .canGoForward(false)
                .background(R.color.md_pink_700)
                .backgroundDark(R.color.md_pink_900)
                .image(R.drawable.img_license)
                .title("For that, we need you to agree to our terms.")
                .description("Lorem ipsum dolor sit amet, sed et laoreet appetere appellantur, eam an dolor audiam similique. Sint principes euripidis sea ne. Ea modus noster ocurreret vix, altera oblique ad vix. Ea nam utamur antiopam, populo iriure saperet id cum. Mei omnis nonumes ne, vis ne enim principes. (DRAFT)")
                .scrollable(true)
                .buttonCtaLabel("I AGREE")
                .buttonCtaClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(IntroActivity.this, "Thank you for agreeing to our terms!", Toast.LENGTH_LONG).show();
                        Intent intentToSignin = new Intent(IntroActivity.this, SignInActivity.class);
                        startActivity(intentToSignin);
                    }
                })
                .build());

    }
}
